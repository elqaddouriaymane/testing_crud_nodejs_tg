import React, { useState } from 'react';
import { validate } from 'react-email-validator';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';

function ForgotPassword() {
  const [isEmailValid, setIsEmailValid] = useState(false);
  const navigate = useNavigate();
  const [msgerr, setMsgerr] = useState('');
  const [form, setForm] = useState({
    email: '',
    password: '',
    rpassword: ''
  });

  const handleEmail = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    if (validate(form.email)) {
      setIsEmailValid(true);
    } else {
      setMsgerr('Votre adresse e-mail est invalide!');
    }
  };

  const updatePassword = async (e) => {
    e.preventDefault();
    try {
      const response = await axios.post('http://localhost:8082/update-password', {
        userId: getUserIdFromEmail(form.email), // Vous devez implémenter cette fonction
        newPassword: form.password
      });
      console.log(response.data); // Afficher le message du serveur
      navigate('/');
    } catch (error) {
      console.error('Erreur lors de la mise à jour du mot de passe :', error);
      // Afficher un message d'erreur à l'utilisateur
    }
  };

  // Fonction pour extraire l'identifiant de l'utilisateur à partir de l'e-mail (à implémenter)
  const getUserIdFromEmail = (email) => {
    // Vous devez implémenter la logique pour récupérer l'identifiant de l'utilisateur à partir de l'e-mail
    // Par exemple, vous pouvez envoyer une requête GET au serveur pour obtenir l'identifiant de l'utilisateur à partir de l'e-mail
    return 123; // Remplacez cela par l'identifiant réel de l'utilisateur
  };

  const forgot = (
    <div>
      <p className='title-area'>Récupérer le mot de passe</p>
      <form className='form-area' onSubmit={handleSubmit}>
        <div className="mb-3 mt-3">
          <input type="email" name='email' onChange={handleEmail} className="form-control" placeholder='Entrez votre e-mail' />
        </div>
        
        {msgerr === '' ? (
          <span></span>
        ) : (
          <div>
            <p className='alert alert-danger border-0'>{msgerr}</p>
          </div>
        )}
        <button type="submit" className="w-100 mb-2">SUIVANT</button>
        <p className='pq'>Un e-mail contenant un lien a été envoyé <br/> à votre adresse e-mail. Veuillez cliquer sur ce <br/>lien pour compléter la procédure de <br/>récupération de votre mot de passe.</p>
      </form>
    </div>
  );

  const change_password = (
    <div>
      <p className='title-area'>Récupérer le mot de passe</p>
      <form className='form-area' onSubmit={updatePassword}>
        <div className='text text-center mt-3 area-credentials'>
          <h4>Mon Email:</h4>
          <p>{form.email}</p>
        </div>
        <div className="mb-1">
          <input type="password" name='password' onChange={handleEmail} value={form.password} className="form-control" placeholder='Entrez votre Mot de Passe' />
        </div>
        <div className="mb-5">
          <input type="password" name='rpassword' onChange={handleEmail} value={form.rpassword} className="form-control" placeholder='Retaptez votre Mot de Passe' />
        </div>
        {msgerr === '' ? (
          <span></span>
        ) : (
          <div>
            <p className='alert alert-danger border-0'>{msgerr}</p>
          </div>
        )}
        <button type="submit" className="w-100 mb-2">RÉCUPÉRER</button>
        <button type="button" className="w-100 mb-3"><Link className='text-decoration-none text text-white' to='/'>Annuler</Link></button>
      </form>
    </div>
  );

  return (
    <div className='SIGNUP d-flex justify-content-center align-items-center'>
      {isEmailValid === false ? (
        forgot
      ) : (
        change_password
      )}
    </div>
  );
}

export default ForgotPassword;
