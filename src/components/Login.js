import React, { useState } from 'react';
import { Link, useNavigate } from 'react-router-dom';
import axios from 'axios';

function Login() {
  const navigate = useNavigate();
  const [form, setForm] = useState({
    email: '',
    password: ''
  });
  const [msgErr, setMsgErr] = useState('');

  const handleSubmit = async (e) => {
    e.preventDefault();
    // Validation du formulaire
    if (form.email.trim() === '' || form.password.trim() === '') {
      setMsgErr('Veuillez remplir tous les champs');
      return;
    }
  
    // Soumission du formulaire à la route de connexion
    try {
      const response = await axios.post('http://localhost:8082/login', form);
      if (response.status === 200 && response.data.message === "Connexion réussie") {
        navigate('/home'); // Redirection si la connexion réussit
      } else {
        setMsgErr('Adresse e-mail ou mot de passe incorrect')
        navigate('/home');
      }
    } catch (error) {
      console.error('Erreur lors de la connexion :', error);
      setMsgErr('Erreur lors de la connexion. Veuillez réessayer.');
    }
  };
  

  const handleInput = (e) => {
    setForm({
      ...form,
      [e.target.name]: e.target.value
    });
  };

  return (
    <div className='LOGIN d-flex justify-content-center align-items-center'>
      <div>
        <div className='mb-3'>
          <img src={"./image/tgr/logo.png"} className='img-area' alt="hero" />
        </div>
        <form className='form-area' onSubmit={handleSubmit}>
          <div className='mb-3'>
            <p className='text text-center'>Se connecter</p>
          </div>
          <div className="mb-3">
            <input type="email" className="form-control" placeholder='Entrez votre Email' onChange={handleInput} name='email' value={form.email} />
          </div>
          <div className="mb-3">
            <input type="password" className="form-control" placeholder='Entrez votre Mot de passe' onChange={handleInput} name='password' value={form.password} />
          </div>
          <div className="mb-2 form-check">
            <label className="form-check-label" htmlFor="exampleCheck1">Mémoriser mot de passe</label>
            <input type="checkbox" className="form-check-input" id="exampleCheck1" />
          </div>
          <div className='mb-3 text text-center'>
            <Link className='text-decoration-none link-area' to='/forgotPassword'>Oublier mot de passe?</Link>
          </div>
          {msgErr && <p className='alert alert-danger border-0'>{msgErr}</p>}
          <button type="submit" className="w-100 mb-2">SE CONNECTER</button>
          <button type="button" className="w-100 mb-3"><Link className='text-decoration-none ' to='/signup'>CRÉER UN COMPTE</Link></button>
        </form>
      </div>
    </div>
  );
}

export default Login;
