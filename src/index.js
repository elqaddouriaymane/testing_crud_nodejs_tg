import React, { StrictMode } from "react";
import ReactDOM from "react-dom/client"
import App from "./App";
import "./App.css";
import { createStore, combineReducers } from 'redux';
import pretsReducer from "./App/reducers/pretsReducer";
import { Provider } from "react-redux";
const rootReducer = combineReducers({
  pretsReducer, // Assurez-vous que vous utilisez le même nom que celui que vous utilisez pour connecter votre composant
  // Autres réducteurs...
});

const store = createStore(rootReducer);



const root=ReactDOM.createRoot(document.querySelector("#router"))
root.render(
    <StrictMode>
   <Provider store={store}>
   <App/>
   </Provider>
            
        
    </StrictMode>
)