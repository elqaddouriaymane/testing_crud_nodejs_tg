// actions/pretsActions.js

import { ADD_PRET ,DELETE_PRET ,UPDATE_PRET } from "./types/pretsTypes";

export const addPret = (pretData) => {
  return {
    type: ADD_PRET,
    payload: pretData,
  };
};

export const deletePret = (pretId) => {
  return {
    type: DELETE_PRET,
    payload: pretId,
  };
};

export const updatePret = (pretData) => {
  return {
    type: UPDATE_PRET,
    payload: pretData,
  };
};
