// reducers/pretsReducer.js

// Actions types
const ADD_PRET = 'ADD_PRET';
const DELETE_PRET = 'DELETE_PRET';
const UPDATE_PRET = 'UPDATE_PRET';

// Initial state
const initialState = {
  prets: [], // Ensure that prets is initialized with an empty array or an appropriate default value
};

const pretsReducer = (state = initialState, action) => {
  switch (action.type) {
    case ADD_PRET:
      return {
        ...state,
        prets: [...state.prets, action.payload],
      };
    case DELETE_PRET:
      return {
        ...state,
        prets: state.prets.filter(pret => pret.id !== action.id),
      };
    case UPDATE_PRET:
      return {
        ...state,
        prets: state.prets.map(pret =>
          pret.id === action.payload.id ? action.payload : pret
        ),
      };
      
    default:
      return state;
  }
};

export default pretsReducer;
