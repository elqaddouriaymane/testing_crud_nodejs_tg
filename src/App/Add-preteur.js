import React from 'react';
import { Link } from 'react-router-dom'; // Import Link from react-router-dom

function Addpreteur() {
  return (
    <div className="app-container">
      <header className='header'>
        <img src={"./image/tgr/fadwa.jpg"} alt="Logo" className="logo" />
      </header>
      <h1>Gestion De La Dette Publique</h1>
      <div className="content">
      
        <div className="navigation">
        <ul>
        <li><Link to="/home"><p className='gg'>Ajouter un prêt</p></Link></li><hr />
        <li><Link to="/add-preteur"> <p className='gg'>Ajouter Prêteur</p></Link></li><hr />
        <li><Link to="/add-devise"><p className='gg'>Ajouter Devise </p></Link></li><hr />
        <li><Link to="/listePret"><p className='gg'>Liste Des prêts </p></Link></li><hr />
        <li><Link to="/listepreteur"><p className='gg'>Liste Preteurs</p></Link></li><hr />
        <li><Link to="/listedevise"><p className='gg'>Liste Devise</p></Link></li><hr />
        <li><Link to="/gerre-compte"><p className='gg'>Configuration du compte</p></Link></li>
      </ul>
        </div>
        <div className='form-container'>
        <form className='form' action="">
        <div className='h3'><h3> Ajouter un Préteur:</h3></div>
        <div className="form-group">
      <label htmlFor="maturite">MATURITE :</label>
      <input type="text" id="maturite" name="maturite" />
    </div>
    <div className="form-group">
      <label htmlFor="Designation">DESIGNATION:</label>
      <input type="text" id="Designation" name="Designation" />
    </div>
    <div className="form-group">
      <label htmlFor="adresse">ADRESSE :</label>
      <textarea id='adress'></textarea>
    </div>
    <button id='button'>Ajouter</button>
    <div className='decnx'><Link   to='/'>Deconnexion</Link></div>
    
  </form>
  </div>
      </div>
    </div>
  );
}

export default Addpreteur;
