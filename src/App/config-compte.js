import React, { useState } from 'react';
import { Link } from 'react-router-dom';

function Configcompte() {
  const [showForm, setShowForm] = useState(false);
  const [password, setPassword] = useState('');
  const [passwordError, setPasswordError] = useState('');
  const [npassword, setnPassword] = useState('');


  const handleSuivantClick = () => {
    // Check if password meets the minimum length requirement
    if (password.length >= 8) {
      setShowForm(true);
      setPasswordError('');
    } else {
      setPasswordError('Le mot de passe doit comporter au moins 8 caractères.');
    }
  };

  const handleChangePassword = (event) => {
    setPassword(event.target.value);
    
  };

  const formu = (
    <div className='form-container'>
      <form className='form'>
        <label htmlFor="email">Votre Email</label>
        <p> aaaaaa@gmail.com</p>
        
        <label htmlFor="NOM">NOM</label>
        <input type="text" id="NOM" name="NOM" /> <br/>
        
        <label htmlFor="prenom">PRENOM</label>
        <input type="text" id="prenom" name="prenom" /> <br/>
        
        <label htmlFor="password">MOT DE PASSE:</label>
        <input type="password" id="password" name="password" value={npassword} onChange={handleChangePassword} /> <br/>
        {passwordError && <p className="error-message">{passwordError}</p>}
        
        <label htmlFor="Rpassword">  Retapez Votre Mot De Passe:</label>
        <input type="password" id="Rpassword" name="Rpassword" /> <br/>
        <button className='button'>Modifier</button>
      </form>
    </div>
  );

  return (
    <div className="app-container">
      <header className='header'>
        <img src="./image/tgr/fadwa.jpg" alt="Logo" className="logo" />
      </header>
      <h1>Gestion De La Dette Publique</h1>
      <div className="content">
        <div className="navigation">
        <ul>
        <li><Link to="/home"><p className='gg'>Ajouter un prêt</p></Link></li><hr />
        <li><Link to="/add-preteur"> <p className='gg'>Ajouter Prêteur</p></Link></li><hr />
        <li><Link to="/add-devise"><p className='gg'>Ajouter Devise </p></Link></li><hr />
        <li><Link to="/listePret"><p className='gg'>Liste Des prêts </p></Link></li><hr />
        <li><Link to="/listepreteur"><p className='gg'>Liste Preteurs</p></Link></li><hr />
        <li><Link to="/listedevise"><p className='gg'>Liste Devise</p></Link></li><hr />
        <li><Link to="/gerre-compte"><p className='gg'>Configuration du compte</p></Link></li>
      </ul>
        </div>
        <form className='form' action="">
          <div className='h3'><h3> Configuration du compte:</h3></div>
          {!showForm && (
            <div className="form-group">
              <label htmlFor="maturite">EMAIL</label>
              <p> aaaaaa@gmail.com</p>
            </div>
          )}
          {showForm && formu}
          {!showForm && (
            <div className="form-group">
              <label htmlFor="valeur">MOT DE PASSE:</label>
              <input type="password" id="valeur" name="valeur" value={password} onChange={handleChangePassword} /> <br/>
              {passwordError && <p className="error-message">{passwordError}</p>}
              <button onClick={handleSuivantClick} id='button'>Suivant</button>
            </div>
          )}
        </form>
        <div className='decnx'><Link to='/'>Deconnexion</Link></div>
      </div>
    </div>
  );
}

export default Configcompte;
