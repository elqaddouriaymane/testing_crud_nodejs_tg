import React, { useState } from 'react';

import 'react-datepicker/dist/react-datepicker.css';
import './Home.css';
import { Link } from 'react-router-dom';
import { connect } from 'react-redux';
import { addPret } from './reducers/pretsActions';

function Home({ addPret }) {
  
    const handleSubmit = (e) => {
      e.preventDefault();
      const formData = {
        dateCreation,
        dateEchange,
        objet,
        devise,
        preteur,
        soldeCourant,
        observation,
        nombreTranche,
        montantCapital,
        montantTinieret,
        montantCommission
      };
      addPret(formData);
    };
  // Définition de l'état pour chaque champ de saisie
  const [dateCreation, setDateCreation] = useState(null);
  const [dateEchange, setDateEchange] = useState(null);
  const [objet, setObjet] = useState('');
  const [preteur , setpreteur]=useState('')
  const [devise , setdevise]=useState('')
  const [soldeCourant, setSoldeCourant] = useState('');
  const [observation, setObservation] = useState('');
  const [nombreTranche, setNombreTranche] = useState('');
  const [montantCapital, setMontantCapital] = useState('');
  const [montantTinieret, setMontantTinieret] = useState('');
  const [montantCommission, setMontantCommission] = useState('');

  // Gestionnaire de changement pour la date de création
  const handleDateCreationChange = (date) => {
    setDateCreation(date);
  };

  // Gestionnaire de changement pour la date d'échange
  const handleDateEchangeChange = (date) => {
    setDateEchange(date);
  
   
  };

  return (
    <div className="app-container">
      <header className='header'>
        <img src={"./image/tgr/fadwa.jpg"} alt="Logo" className="logo" />
      </header>
      <h1>Gestion De La Dette Publique</h1>
      <div className="content">
        <div className="navigation">
        <ul>
        <li><Link to="/home"><p className='gg'>Ajouter un prêt</p></Link></li><hr />
        <li><Link to="/add-preteur"> <p className='gg'>Ajouter Prêteur</p></Link></li><hr />
        <li><Link to="/add-devise"><p className='gg'>Ajouter Devise </p></Link></li><hr />
        <li><Link to="/listePret"><p className='gg'>Liste Des prêts </p></Link></li><hr />
        <li><Link to="/listepreteur"><p className='gg'>Liste Preteurs</p></Link></li><hr />
        <li><Link to="/listedevise"><p className='gg'>Liste Devise</p></Link></li><hr />
        <li><Link to="/gerre-compte"><p className='gg'>Configuration du compte</p></Link></li>
      </ul>
        </div>

        <div className='form-container'>
          <form className='form'  onSubmit={handleSubmit} method='POST'>
            <div className='h3'><h3>Ajouter un Prêt :</h3></div>
            <div className="form-group">
              <label htmlFor="dateCreation">Date Creation :</label>
              <input type='date' id='dateCreaction' name='dateCreaction' value={dateCreation} onChange={(e) => setDateCreation(e.target.value)} />
            </div>
            <div className="form-group">
              <label htmlFor="objet">Objet :</label>
              <input type="text" id="objet" name="objet" value={objet} onChange={(e) => setObjet(e.target.value)} />
            </div>
            <div className="form-group">
              <label htmlFor="soldeCourant">Solde Courant :</label>
              <input type="text" id="soldeCourant" name="soldeCourant" value={soldeCourant} onChange={(e) => setSoldeCourant(e.target.value)} />
            </div>
            <div className="form-group">
              <label htmlFor="observation">Observation :</label>
              <input type="text" id="observation" name="observation" value={observation} onChange={(e) => setObservation(e.target.value)} />
            </div>
            <div className="form-group">
              <label htmlFor="preteur">Préteur :</label>
              <input type="text" id="preteur" name="preteur" value={preteur} onChange={(e) => setpreteur(e.target.value)} />
            </div>
            <div className="form-group">
              <label htmlFor="devise">Devise :</label>
              <input type="text" id="devise" name="devise" value={devise} onChange={(e) => setdevise(e.target.value)} />
              
            </div>
            <div className="form-group">
              <label htmlFor="nombreTranche">Nombre tranche :</label>
              <input type="text" id="nombreTranche" name="nombreTranche" value={nombreTranche} onChange={(e) => setNombreTranche(e.target.value)} />
            </div>
           
              <div className="form-group">
              <label htmlFor="dateEchange">Date ENCHANGE :</label>
              <input type='date' id='dateEchange' name='dateEchange' value={dateEchange} onChange={(e) => setDateEchange(e.target.value)} />
            </div>
            
            
            <div className="form-group">
              <label htmlFor="montantCapital">Montant Capitale :</label>
              <input type="text" id="montantCapital" name="montantCapital" value={montantCapital} onChange={(e) => setMontantCapital(e.target.value)} />
            </div>
            <div className="form-group">
              <label htmlFor="montantTinieret">Montant Tinieret :</label>
              <input type="text" id="montantTinieret" name="montantTinieret" value={montantTinieret} onChange={(e) => setMontantTinieret(e.target.value)} />
            </div>
            <div className="form-group">
              <label htmlFor="montantCommission">Montant Comission :</label>
              <input type="text" id="montantCommission" name="montantCommission" value={montantCommission} onChange={(e) => setMontantCommission(e.target.value)} />
            </div>
            <button id='button' type='submit'>Ajouter</button>
            <div className='decnx'><Link to='/'>Deconnexion</Link></div>
          </form>
        </div>
      </div>
    </div>
  );
}

const mapDispatchToProps = (dispatch) => ({
  addPret: (pretData) => dispatch(addPret(pretData)),
});

export default connect(null, mapDispatchToProps)(Home);
