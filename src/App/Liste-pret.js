import React from 'react';
import { connect } from 'react-redux';
import { Link } from 'react-router-dom';
import { deletePret } from './reducers/pretsActions'; 
import { updatePret } from './reducers/pretsActions';// Import de l'action de suppression

function Listepret({ prets, deletePret }) {
  
  const handleDelete = (id) => {
    deletePret(id);
  };
  const handlUPDATE = (id)=>{

    updatePret(id)
  }
  return (
    <div className="app-container">
      <header className='header'>
        <img src={"./image/tgr/fadwa.jpg"} alt="Logo" className="logo" />
      </header>
      <h1>Gestion De La Dette Publique</h1>
      <div className="content">
        <div className="navigation">
        <ul>
        <li><Link to="/home"><p className='gg'>Ajouter un prêt</p></Link></li><hr />
        <li><Link to="/add-preteur"> <p className='gg'>Ajouter Prêteur</p></Link></li><hr />
        <li><Link to="/add-devise"><p className='gg'>Ajouter Devise </p></Link></li><hr />
        <li><Link to="/listePret"><p className='gg'>Liste  Prêts </p></Link></li><hr />
        <li><Link to="/listepreteur"><p className='gg'>Liste Preteurs</p></Link></li><hr />
        <li><Link to="/listedevise"><p className='gg'>Liste Devise</p></Link></li><hr />
        <li><Link to="/gerre-compte"><p className='gg'>Configuration du compte</p></Link></li>
      </ul>
        </div>
        <div className='form-container'>
          <div className='h3'><h3> liste des  Préts:</h3></div>
          <div style={{textAlign: "end"}}> <forme>
              <input  type='text' placeholder='Entrez vote Prêteur '/>
              <button>  Recherche</button>
           </forme>
           </div>
          <table>
            <thead>
              <tr>
                <th >ID</th>
                <th>Date de création</th>
                <th>Objet</th>
                <th>Solde Courant</th>
                <th>Observation</th>
                <th>Prêteur</th>
                <th>Devise</th>
                <th>Nombre  Tranche</th>
                <th>Date ENCHANGE</th>
                <th>Montant Capitale</th>
                <th>Montant Tinieret</th>
                <th>Montant Comission</th>
                <th>Actions</th> {/* Colonne pour les actions */}
              </tr>
            </thead>
            <tbody>
              {prets.map((pret, index) => (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{pret.dateCreation}</td>
                  <td>{pret.objet}</td>
                  <td>{pret.soldeCourant}</td>
                  <td>{pret.observation}</td>
                  <td>{pret.preteur}</td>
                  <td>{pret.devise}</td>
                  <td>{pret.nombreTranche}</td>
                  <td>{pret.dateEchange}</td>
                  <td>{pret.montantCapital}</td>
                  <td>{pret.montantTinieret}</td>
                  <td>{pret.montantCommission}</td>
                  <td>
                    <button onClick={() => handleDelete(pret.id)}>Supprimer</button>
                    <button onClick={() => handlUPDATE(pret.id)}>Modifier</button>
                    {/* Ajout du bouton Supprimer */}
                    {/* Ajoutez ici un bouton Modifier et associez-le à une fonction de modification */}
                  </td>
                </tr>
              ))}
            </tbody>
          </table>
        </div>
      </div>
    </div>
  );
}

const mapStateToProps = (state) => ({
  prets: state.pretsReducer.prets,
});

const mapDispatchToProps = (dispatch) => ({
  deletePret: (id) => dispatch(deletePret(id)),
});

export default connect(mapStateToProps, mapDispatchToProps)(Listepret);
