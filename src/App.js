import React from 'react'
import { BrowserRouter, Route, Routes } from 'react-router-dom'
import Login from './components/Login'
import Signup from './components/Signup'
import ForgotPassword from './components/ForgotPassword'

import Addpreteur from './App/Add-preteur'
import Adddevise from './App/Add-devise'
import Listepret from './App/Liste-pret'
import Listepreteur from './App/Liste-preteur'
import Listedevise from './App/Liste-devise'
import Configcompte from './App/config-compte'
import Home from './App/Home'

function App() {
  return (
    <div>
    <BrowserRouter>
      <Routes>
        <Route path='/' element={<Login/>}/>
        <Route path='/signup' element={<Signup/>}/>
        <Route path='/forgotPassword' element={<ForgotPassword/>}/>
        <Route path='/home' element={<Home/>}/>
        
        <Route path="/add-preteur" element={<Addpreteur/>} />
        <Route path="/add-devise" element={<Adddevise/>} />
        <Route path="/listePret" element={<Listepret/>} />
        <Route path="/listepreteur" element={<Listepreteur/>} />
        <Route path="/listedevise" element={<Listedevise/>} />
        <Route path="/gerre-compte" element={<Configcompte/>} />
      </Routes>
      </BrowserRouter>
    </div>
  )
}

export default App 