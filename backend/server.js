const express = require('express');
const mysql = require('mysql');
const cors = require('cors');

const app = express();
app.use(cors());
app.use(express.json());

const db = mysql.createConnection({
  host: 'localhost',
  user: 'root',
  password: '',
  database: 'signup'
});

db.connect((err) => {
  if (err) {
    console.error('Erreur de connexion à la base de données MySQL :', err);
  } else {
    console.log('Connexion à la base de données MySQL réussie');
  }
});

app.post('/signup', (req, res) => {
  const { nom, prenom, email, password } = req.body;
  const sql = "INSERT INTO login (nom, prenom, email, password) VALUES (?, ?, ?, ?)";
  const values = [nom, prenom, email, password];

  db.query(sql, values, (err, result) => {
    if (err) {
      console.error('Erreur lors de l\'insertion des données :', err);
      res.status(500).json({ error: 'Erreur lors de l\'inscription' });
    } else {
      console.log('Données insérées avec succès');
      res.status(200).json({ message: 'Inscription réussie' });
    }
  });
});

app.post('/login', (req, res) => {
  const { email, password } = req.body;
  
  if (!email || !password) {
    return res.status(400).json({ error: "Veuillez fournir une adresse e-mail et un mot de passe." });
  }

  const sql = "SELECT * FROM login WHERE email = ?";
  db.query(sql, [email], (err, results) => {
    if (err) {
      console.error('Erreur lors de la recherche de l\'utilisateur :', err);
      return res.status(500).json({ error: "Erreur lors de la recherche de l'utilisateur." });
    }
    
    if (results.length === 0) {
      return res.status(404).json({ error: "Utilisateur non trouvé." });
    }
    
    const user = results[0];
    
    if (user.password !== password) {
      return res.status(401).json({ error: "Mot de passe incorrect." });
    }

    // Si l'email et le mot de passe correspondent, vous pouvez autoriser l'accès.
    res.status(200).json({ message: "Connexion réussie." });
  });
});


app.listen(8082, () => {
  console.log('Serveur en écoute sur le port 8082');
});
